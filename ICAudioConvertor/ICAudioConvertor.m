//
//  ICAudioConvertor.m
//  ICAudioConvertor
//
//  Created by Matthew Casey on 04/11/2012.
//  Copyright (c) 2012 iOSConsultancy.co.uk. All rights reserved.
//

#import "ICAudioConvertor.h"

#define checkResult(result,operation) (_checkResult((result),(operation),__FILE__,__LINE__))
static inline BOOL _checkResult(OSStatus result, const char *operation, const char* file, int line) {
    if ( result != noErr ) {
        NSLog(@"%s:%d: %s result %d %08X %4.4s\n", file, line, operation, (int)result, (int)result, (char*)&result);
        return NO;
    }
    return YES;
}



@implementation ICAudioConvertor
@synthesize audioConvertor = audioConvertor;

+(ICAudioConvertor *)convertorWithDelegate:(id<ICAudioConvertorDelegate>)delegate {
    ICAudioConvertor *audioConvertor = [[ICAudioConvertor alloc] init];
    audioConvertor.delegate = delegate;
    return audioConvertor;
}

-(ICAudioConvertorStarterState)convertAudioFrom:(NSString *)fromFile to:(NSString *)toFile {
    if ( ![TPAACAudioConverter AACConverterAvailable] ) {
        return ICAudioConvertorUnsupportedDevice;
    }
    /*Uncomment this code if you would like us to build the audio session
    // Initialise audio session, and register an interruption listener, important for AAC conversion
    if ( !checkResult(AudioSessionInitialize(NULL, NULL, interruptionListener, (__bridge void*) self), "initialise audio session") ) {
        return  ICAudioConvertorCouldntInitAudioSession;
    }
    
    
    // Set up an audio session compatible with AAC conversion.  Note that AAC conversion is incompatible with any session that provides mixing with other device audio.
    UInt32 audioCategory = kAudioSessionCategory_MediaPlayback;
    if ( !checkResult(AudioSessionSetProperty(kAudioSessionProperty_AudioCategory, sizeof(audioCategory), &audioCategory), "setup session category") ) {
        return ICAudioConvertorCouldntSetupAudioCategory;
    }
     */
    self.destination = toFile;
    self.source = fromFile;
    self.audioConvertor = [[TPAACAudioConverter alloc] initWithDelegate:self
                                                            source:fromFile
                                                       destination:toFile];
    
    [self.audioConvertor start];
    return ICAudioConvertorStarted;
}




static void interruptionListener(void *inClientData, UInt32 inInterruption)
{
	ICAudioConvertor *THIS = (__bridge ICAudioConvertor *)inClientData;
	
	if (inInterruption == kAudioSessionEndInterruption) {
		// make sure we are again the active session
		checkResult(AudioSessionSetActive(true), "resume audio session");
        if ( THIS->audioConvertor ) [THIS->audioConvertor resume];
	}
	
	if (inInterruption == kAudioSessionBeginInterruption) {
        if ( THIS->audioConvertor ) [THIS->audioConvertor interrupt];
    }
}

#pragma mark - Audio converter delegate

-(void)AACAudioConverter:(TPAACAudioConverter *)converter didMakeProgress:(CGFloat)progress {
    //self.progressView.progress = progress;
    [self.delegate convertor:self madeProgress:progress];
}

-(void)AACAudioConverterDidFinishConversion:(TPAACAudioConverter *)converter {
    self.audioConvertor = nil;
    [self.delegate convertorDidComplete:self];
}

-(void)AACAudioConverter:(TPAACAudioConverter *)converter didFailWithError:(NSError *)error {
    self.audioConvertor = nil;
    [self.delegate convertor:self didFailWithError:error];
}
@end
