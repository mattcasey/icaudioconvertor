//
//  ICAudioConvertor.h
//  ICAudioConvertor
//
//  Created by Matthew Casey on 04/11/2012.
//  Copyright (c) 2012 iOSConsultancy.co.uk. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TPAACAudioConverter.h"
typedef enum ICAudioConvertorStarterState {
    ICAudioConvertorUnsupportedDevice = 0,
    ICAudioConvertorCouldntInitAudioSession,
    ICAudioConvertorCouldntSetupAudioCategory,
    ICAudioConvertorStarted
} ICAudioConvertorStarterState;
@class ICAudioConvertor;
@protocol ICAudioConvertorDelegate
-(void)convertor:(ICAudioConvertor *)convertor madeProgress:(CGFloat)progress;
-(void)convertorDidComplete:(ICAudioConvertor *)convertor;
-(void)convertor:(ICAudioConvertor *)convertor didFailWithError:(NSError *)error;
@end
@interface ICAudioConvertor : NSObject <TPAACAudioConverterDelegate>
@property (nonatomic, assign) id <ICAudioConvertorDelegate> delegate;
@property (nonatomic, strong) TPAACAudioConverter *audioConvertor;
@property (nonatomic, strong) NSString *destination;
@property (nonatomic, strong) NSString *source;
+(ICAudioConvertor *)convertorWithDelegate:(id<ICAudioConvertorDelegate>)delegate;
-(ICAudioConvertorStarterState)convertAudioFrom:(NSString *)fromFile to:(NSString *)toFile;
@end
