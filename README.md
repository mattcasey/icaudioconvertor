# Introduction
 
ICAudioConvertor gives you an easy way to convert files recorded using the iphone into a much smaller file format. 

It is a simple wrapper around the work of:

https://github.com/michaeltyson/TPAACAudioConverter

#Installation

[1]: Either add the project as a git submodule or download a zip file. 

[2]: Then add the project as a sub project to your Xcode project. 

[3]: Once done, add a target dependency for the library and link against this library. 

[4]: Be sure to link against AudioToolbox as well. 

[5]: Then add this to your header search paths: 

$(BUILT_PRODUCTS_DIR)/../../Headers